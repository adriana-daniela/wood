/**
 *
 * Asynchronously loads the component for About
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
